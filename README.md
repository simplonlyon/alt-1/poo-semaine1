# Résumé première semaine

Le projet est pas très bien organisé, mais il contient :

## Arbre de décision 
Une représentation sous forme de classes d'un arbre de décision qui pourrait être utilisé pour un "livre dont vous êtes le héro", ou pour 
un questionnaire, composé d' Evenement et de Choix représentés [Ici](https://gitlab.com/simplonlyon/alt-1/poo-semaine1/-/tree/master/src/main/java/co/simplon/alt1/data)

Il était ensuite demandé de créer un [Parser](https://gitlab.com/simplonlyon/alt-1/poo-semaine1/-/tree/master/src/main/java/co/simplon/alt1/parser) permettant de transformer [ces data en json](https://gitlab.com/simplonlyon/alt-1/poo-semaine1/-/blob/master/src/main/resources/dataset.json) en instances des classes ci dessus

Et enfin de créer des classes permettant l'interaction avec l'utilisateur.ice via la ligne de commande [ici](https://gitlab.com/simplonlyon/alt-1/poo-semaine1/-/blob/master/src/main/java/co/simplon/alt1/interaction/GameConsole.java)

## Un Sudoku Solver
Pas de solution pour ça dans ce dépôt, mais il y en a une dans [celui ci](https://gitlab.com/simplonlyon/promo12/cpro/poo-semaine1/-/blob/master/src/Solver/Sudoku.php) d'une classe permettant de solutionner un Sudoku représenté sous forme de tableau comme avec [cet exemple](https://gitlab.com/simplonlyon/promo12/cpro/poo-semaine1/-/blob/master/public/exo-sudoku.php)


## Un Morpion + IA
Ensuite il était demandé de créer un [jeu de morpion/TicTacToe](https://gitlab.com/simplonlyon/alt-1/poo-semaine1/-/tree/master/src/main/java/co/simplon/alt1/tictactoe) en ligne de commande puis de créer plusieurs "IA adversaires" , une qui joue de manière aléatoire et une qui joue en analysant la grille (pas d'exemple de code pour ça non plus, mais vos camarades qui étaient là peuvent vous transmettre les leurs)