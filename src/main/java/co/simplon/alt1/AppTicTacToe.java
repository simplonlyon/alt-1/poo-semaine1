package co.simplon.alt1;

import co.simplon.alt1.interaction.IGame;
import co.simplon.alt1.interaction.TicTacToeConsole;
import co.simplon.alt1.player.ConsolePlayer;

public class AppTicTacToe {
    public static void main(String[] args) {
        IGame game = new TicTacToeConsole(new ConsolePlayer(), new ConsolePlayer());

        game.play();
        
    }
}
