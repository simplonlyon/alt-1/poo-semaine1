package co.simplon.alt1.interaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


import co.simplon.alt1.data.Event;

public class GameConsole implements IGame {

    private Event event;

    public GameConsole(Event event) {
        this.event = event;
    }

    @Override
    public void play() {

        System.out.println(this.event.getText());
        List<String> choices = this.event.listChoices();

        if (!choices.isEmpty()) {
            for (int i = 0; i < choices.size(); i++) {
                String choice = choices.get(i);
                System.out.println(i+" : "+choice);
                
            }
        

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            try {
                String input = reader.readLine();
                this.event = this.event.choose(Integer.parseInt(input));
                this.play();
            } catch (IOException e) {
                
                e.printStackTrace();
            } catch(NumberFormatException e) {
                System.out.println("Bad input, please enter your choice index");
                this.play();
            } catch(IndexOutOfBoundsException e) {
                System.out.println("Bad input, please choose between available indexes");
                this.play();
            }
        }

    }

}
