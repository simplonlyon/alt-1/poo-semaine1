package co.simplon.alt1.interaction;

import co.simplon.alt1.player.IPlayer;
import co.simplon.alt1.tictactoe.GridInspector;
import co.simplon.alt1.tictactoe.TicTacToe;

public class TicTacToeConsole implements IGame{

    private TicTacToe board;
    private GridInspector inspector;
    private IPlayer[] players = new IPlayer[2];

    public TicTacToeConsole(IPlayer player1, IPlayer player2) {
        this.board = new TicTacToe();
        this.inspector = new GridInspector();
        this.players[0] = player1;
        this.players[1] = player2;
    }
    

    @Override
    public void play() {
        for (IPlayer player : this.players) {
            player.playerMove(board);

            System.out.println(this.board.display());
            Character winner = this.inspector.winningGrid(board);
            if(winner != null) {
                System.out.println(winner+" has won the game");
                return;
            }
            if(this.inspector.isFull(board)) {
                System.out.println("Draw");
                return;
            }
        }
        this.play();
    }
    
}
