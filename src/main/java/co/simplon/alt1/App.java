package co.simplon.alt1;


import java.util.Scanner;


import co.simplon.alt1.data.Event;
import co.simplon.alt1.interaction.GameConsole;
import co.simplon.alt1.interaction.IGame;
import co.simplon.alt1.parser.IEventParser;
import co.simplon.alt1.parser.JsonEventParser;


public class App {
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(App.class.getClassLoader().getResourceAsStream("dataset.json"), "UTF-8").useDelimiter("\\A");
        String text = scanner.next();
        scanner.close();
        
        IEventParser parser = new JsonEventParser();

        Event event = parser.parse(text);

        IGame game = new GameConsole(event);

        game.play();
        
    }
}
