package co.simplon.alt1.player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import co.simplon.alt1.tictactoe.TicTacToe;

public class ConsolePlayer implements IPlayer {

    @Override
    public void playerMove(TicTacToe board) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Select your tic position");
            try {
                String input = reader.readLine();
                String[] coords = input.split(":");
                if(!board.tic(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]))) {
                    System.out.println("Bad move.");
                    this.playerMove(board);
                }
            } catch (IOException e) {
                
                e.printStackTrace();
            }catch(NumberFormatException e) {
                System.out.println("Bad input, please enter format 1:1");
                this.playerMove(board);
            }

    }
    
}
