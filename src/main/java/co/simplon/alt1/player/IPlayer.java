package co.simplon.alt1.player;

import co.simplon.alt1.tictactoe.TicTacToe;

public interface IPlayer {
    void playerMove(TicTacToe board);
}
