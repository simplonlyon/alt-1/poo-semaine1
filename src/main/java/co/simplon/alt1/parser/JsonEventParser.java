package co.simplon.alt1.parser;

import com.google.gson.Gson;

import co.simplon.alt1.data.Choice;
import co.simplon.alt1.data.Event;
import co.simplon.alt1.parser.json.ChoiceJson;
import co.simplon.alt1.parser.json.EventJson;

public class JsonEventParser implements IEventParser {

    private EventJson[] result;

    @Override
    public Event parse(String input) {
        Gson gson = new Gson();

        this.result = gson.fromJson(input, EventJson[].class);
        
        return this.parseEvent(this.result[0]);

        
        
    }

    private Event parseEvent(EventJson eventJson) {

        Event event = new Event(eventJson.text);

        for (ChoiceJson choiceJson : eventJson.choices) {

            Choice choice = new Choice(choiceJson.text, this.parseEvent(this.find(choiceJson.next)));
            event.addChoice(choice);
        }

        return event;
    }
    

    private EventJson find(Integer index) {
        
        for (EventJson eventJson : this.result) {
            if(eventJson.id == index) {
                return eventJson;
            }
        }
        return null;
    }
}
