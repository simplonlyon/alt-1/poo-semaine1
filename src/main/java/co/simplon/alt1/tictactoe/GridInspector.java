package co.simplon.alt1.tictactoe;



public class GridInspector {
    
    public Character winningGrid(TicTacToe ticTacToe) {
        Character[][][] allRows = {
            ticTacToe.getColumns(),
            ticTacToe.getRows(),
            ticTacToe.getDiags()
        };
        
        for (Character[][] rows : allRows) {

            for (Character[] row : rows) {
                Character firstChar = row[0];
                int counter = 0;
                for (Character symbol : row) {
                    if(symbol == firstChar) {
                        counter++;
                    }
                }
                if(counter == 3) {
                    return firstChar;
                }

            }

        }

        return null;
    }
    
    public boolean isFull(TicTacToe ticTacToe) {
        for (Character[] row : ticTacToe.getRows()) {
            for (Character symbol : row) {
                if(symbol == null) {
                    return false;
                }
            }
        }
        return true;
    }
}
