package co.simplon.alt1.tictactoe;

public class TicTacToe {
    private Character[][] grid = new Character[3][3];
    private boolean turnX = false;

    public boolean tic(int row, int col) {

        try {
            if (grid[row][col] == null) {

                if (this.turnX) {
                    grid[row][col] = 'X';
                } else {
                    grid[row][col] = 'O';
                }
                this.turnX = !this.turnX;
                return true;

            }
        } catch (IndexOutOfBoundsException e) {
            return false;

        }
        return false;
    }

    public String display() {
        String gridDisplay = "";
        for (Character[] row : this.grid) {
            for (Character cell : row) {
                gridDisplay += cell != null ? cell : "_";

            }
            gridDisplay += "\n";
        }

        return gridDisplay;
    }

    /**
     * 
     * getColumns
     * getRows
     * getDiags
     */

     public Character[][] getColumns() {
        Character[][] cols = new Character[3][3];
        for(int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {
                cols[i][j] = this.grid[j][i];
            }
            
        }
        return cols;
     }

     public Character[][] getDiags() {
        Character[][] diags = new Character[2][3];
        for(int i = 0; i < this.grid.length; i++) {
            for (int j = 0; j < this.grid[i].length; j++) {
                if(i == j) {
                    diags[0][i] = this.grid[i][j];
                }
                if(i+j == this.grid.length-1) {
                    diags[1][i] = this.grid[i][j];
                }
            }
        } 
        return diags;
     }

     public Character[][] getRows() {
        return this.grid.clone();
     }


     /*
     public Character[][] getManualDiags() {
         return {
             {this.grid[0][0], this.grid[1][1], this.grid[2][2]},
             {this.grid[0][2], this.grid[1][1], this.grid[2][0]}
         };
     }
     */
}
